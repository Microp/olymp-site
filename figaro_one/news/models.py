from django.db import models

# Create your models here.

class News(models.Model):

    class Meta():
        #app_label = "News"
        pass


    news_header = models.CharField(max_length=50)
    news_text = models.TextField()
    news_date = models.DateTimeField()


    def get_date_day(self):
        return self.news_date.day

    def get_date_month(self):
        return self.news_date.month

    def get_year(self):
        return self.news_date.year % 100

    def __unicode__(self):
        return "New"

