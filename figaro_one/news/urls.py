
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^page/(?P<num_page_news>\d+)/$', views.news_by_page),
    url(r'^', views.news_all),
]
