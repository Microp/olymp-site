from django.contrib import auth
from django.core.paginator import Paginator
from django.shortcuts import render, render_to_response
from django.template.context_processors import csrf

from .models import News


# Create your views here.


def news_all(request):
    args = {}

    args.update(csrf(request))

    news = News.objects.all().order_by('-news_date')
    paginator = Paginator(news, 5)


    args['user'] = auth.get_user(request)
    args['news'] = paginator.page(1)
    return render_to_response('news.html', args)


def news_by_page(request, num_page_news=1):
    args = {}

    args.update(csrf(request))

    news = News.objects.all().order_by('-news_date')
    paginator = Paginator(news, 5)

    args['page_range'] = paginator.page_range
    args['user'] = auth.get_user(request)
    args['news'] = paginator.page(num_page_news)

    return render_to_response('news.html', args)
