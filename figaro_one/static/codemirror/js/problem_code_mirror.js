/**
 * Created by alexander on 16/01/2017.
 */

$(document).ready(function () {


    var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
        theme: 'neat',
        lineNumbers: true,
        mode: 'python',
        matchBrackets: true,

    });

    myCodeMirror.on("change", function (cm) {
        cm.save()

    });


});
