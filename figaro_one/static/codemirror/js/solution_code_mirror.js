/**
 * Created by alexander on 16/01/2017.
 */

$(document).ready(function () {
    $("source_code_dis").val()
    var resultSolutionCodeMirror = CodeMirror.fromTextArea(source_code_dis, {
        theme: 'neat',
        lineNumbers: true,
        mode: 'python',
        readOnly: "nocursor",
    });

});
