

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from django import forms
from django.utils.html import strip_tags


class UserCreateForm(UserCreationForm):

    field_order = ['username', 'first_name', 'email', 'password1', 'password2']
    username = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Input your Username'}))

    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Input your First_name'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Input your Email' }))


    password1 = forms.CharField(required=True, widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder' : 'Your password'}))
    password2 = forms.CharField(required=True, widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder' : 'Confirm password'}))


    def is_valid(self):
        form = super(UserCreateForm, self).is_valid()
        for f, error in self.errors.items():
            if f != 'password1' and f != 'password2':
                self.fields[f].widget.attrs.update({'value': strip_tags(error)})
        return form


    class Meta():

        fields = ['first_name', 'email', 'username', 'password1', 'password2']


        model = User


