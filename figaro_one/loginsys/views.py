from django.contrib import auth
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from loginsys.forms import UserCreateForm
from django.template.context_processors import csrf


# Create your views here.


def sign_in(request):
    next_page = request.POST.get('HTTP_REFERER')

    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')

        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)

    return HttpResponseRedirect(next_page)


def sign_out(request):
    logout(request)
    next_page = request.POST.get('HTTP_REFERER')
    return HttpResponseRedirect(next_page)


def sign_up(request):

    args = {}
    args.update(csrf(request))

    if auth.get_user(request).username:
        return redirect('/')

    args['form'] = UserCreateForm()

    if request.POST:
        new_user_form = UserCreateForm(request.POST)

        if new_user_form.is_valid():
            new_user_form.save()
            new_user = authenticate(username=new_user_form.cleaned_data['username'],
                                    password=new_user_form.cleaned_data['password2'])

            login(request, new_user)
            return redirect('/')
        else:
            args['form'] = new_user_form

    return render_to_response('sign_up.html', args)
