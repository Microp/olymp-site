from loginsys import views
from django.conf.urls import url

urlpatterns = [
    url(r'^sign_in/$', views.sign_in),
    url(r'^sign_out/$', views.sign_out),
    url(r'^sign_up/$', views.sign_up),
]