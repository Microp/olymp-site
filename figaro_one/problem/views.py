from django.contrib import auth
from django.core.paginator import Paginator
from django.shortcuts import render, render_to_response, redirect
from django.template.context_processors import csrf

from problem.models import Problem, Example, Solution, Solution_result_by_test
from problem.forms import CheckSolutionForm


# Create your views here.

def problems_all(request):
    args = {}
    args.update(csrf(request))

    problems = Problem.objects.all().order_by('id')
    paginator = Paginator(problems, 50)

    args['user'] = auth.get_user(request)
    args['mail'] = 'А вот хренаса тебе'
    args['problems'] = paginator.page(1)

    return render_to_response('problem.html', args)


def problems_by_id(request, problem_id):
    args = {}
    args.update(csrf(request))

    args['form'] = CheckSolutionForm
    args['problem'] = Problem.objects.get(id=problem_id)
    args['user'] = auth.get_user(request)
    args['examples'] = Example.objects.filter(example_problem=problem_id).order_by('example_number')

    return render_to_response('problem_by_id.html', args)


def system_status(request):
    args = {}

    args['solutions'] = Solution.objects.all().order_by('-id')
    args['name'] = 'Anideshi'
    args['user'] = auth.get_user(request)

    return render_to_response('status.html', args)


def send_code_to_check(request):
    if request.POST:

        form = CheckSolutionForm(request.POST)

        if form.is_valid():
            c = form.save(commit=False)
            c.solution_author = auth.get_user(request)
            c.solution_problem_number = Problem.objects.get(id=request.POST['solution_problem_number'])

            form.save()

    args = {}
    # args['source_code'] = request.POST['solution_source_code']
    # return render_to_response('status_solution.html', args)
    return redirect('/problems/status/')


def status_solution(request, solution_id):
    args = {}
    args['solution'] = Solution.objects.get(id=solution_id)
    args['result_by_test'] = Solution_result_by_test.objects.filter(solution_by_test_solution=solution_id).order_by(
        'solution_by_test_test')

    return render_to_response('status_solution.html', args)
