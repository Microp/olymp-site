from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Problem(models.Model):

    problem_name = models.CharField(max_length=50)
    problem_memory_limit = models.IntegerField(default=16)
    problem_time_limit = models.IntegerField(default=1)
    problem_description_legend = models.TextField()

    problem_description_input = models.TextField()
    problem_description_output = models.TextField()

    def __str__(self):
        return self.problem_name


class Example(models.Model):

    example_number = models.IntegerField()
    example_input_txt = models.TextField()
    example_output_txt = models.TextField()
    example_problem = models.ForeignKey(Problem)

    def __str__(self):
        return '%s  %d' % (self.example_problem.problem_name, self.example_number)



class Solution(models.Model):


    solution_author = models.ForeignKey(User)
    solution_problem_number = models.ForeignKey(Problem)
    solution_language = models.CharField(max_length=50)
    solution_source_code = models.TextField(default='', null=True)
    solution_result = models.CharField(max_length=50, null=True)
    solution_test = models.IntegerField(null=True)
    solution_time = models.FloatField(null=True)
    solution_memory = models.CharField(max_length=20, null=True)
    solution_date = models.DateTimeField(auto_now_add=True)


class Solution_result_by_test(models.Model):

    solution_by_test_solution = models.ForeignKey(Solution)
    solution_by_test_test = models.IntegerField()
    solution_by_test_result = models.CharField(max_length=50)
    solution_by_test_time = models.FloatField()
    solution_by_test_memory = models.CharField(max_length=20)