from django.conf.urls import url
from problem import views

urlpatterns = [
    url(r'^(?P<problem_id>\d+)/$', views.problems_by_id),
    url(r'^status/$', views.system_status),
    url(r'^status/solution/(?P<solution_id>\d+)/$', views.status_solution),
    url(r'^send_solution/$', views.send_code_to_check),
    url(r'$', views.problems_all),
]