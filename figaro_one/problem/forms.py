
from django.forms import ModelForm
from django import forms

from problem.models import Solution

class CheckSolutionForm(ModelForm):

    choices_language = (('Python','Python',),('C++', 'C++'))

    solution_language = forms.ChoiceField(required=True, choices=choices_language, widget=forms.Select(attrs={'class':'form-control'}))

    solution_source_code = forms.CharField(required=True, widget=forms.Textarea(attrs={'id':'myTextArea'}))

    class Meta():
        model = Solution

        fields = ['solution_language', 'solution_source_code']