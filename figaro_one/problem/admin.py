from django.contrib import admin
from problem.models import Problem, Example


# Register your models here.


class ExampleAdmin(admin.ModelAdmin):
    fields = ['example_number', 'example_problem', 'example_input_txt', 'example_output_txt']


admin.site.register(Example, ExampleAdmin)
admin.site.register(Problem)
